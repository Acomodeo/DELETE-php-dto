Containse PHP DTO Obejcts

Installation
============

Download the Bundle
---------------------------

Add the following repository to your composer.json
```php
    "repositories" : [
        {
            "type" : "vcs",
            "url" : "https://gitlab.com/Acomodeo/php-dto.git"
        }
    ],
```
Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```bash
$ composer require acomodeo/php-dto "0.1.*"
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

