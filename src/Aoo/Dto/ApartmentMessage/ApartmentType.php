<?php

namespace Aoo\Dto\ApartmentMessage;

use JMS\Serializer\Annotation as JMS;
use Aoo\Dto;

/**
 * Class ApartmentType
 * @package Aoo\Dto
 */
class ApartmentType
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("payloadType")
     * @JMS\Groups({"portal"})
     */
    protected $payloadType;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("messageType")
     * @JMS\Groups({"portal"})
     */
    protected $messageType;

    /**
     * @var Dto\ApartmentType
     * @JMS\Type("Aoo\Dto\ApartmentType")
     * @JMS\SerializedName("payload")
     * @JMS\Groups({"portal"})
     */
    protected $payload;


    /**
     * Delete constructor.
     */
    public function __construct()
    {
        $this->payloadType = 'APARTMENT_TYPE';
        $this->messageType = 'PUT';
    }

    /**
     * @return string
     */
    public function getPayloadType()
    {
        return $this->payloadType;
    }

    /**
     * @return string
     */
    public function getMessageType()
    {
        return $this->messageType;
    }

    /**
     * @return Dto\ApartmentType
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param Dto\ApartmentType $payload
     * @return $this
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;
        return $this;
    }
}
