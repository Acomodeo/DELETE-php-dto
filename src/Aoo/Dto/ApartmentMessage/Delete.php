<?php

namespace Aoo\Dto\ApartmentMessage;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Delete
 * @package Aoo\Dto
 */
class Delete
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("messageType")
     * @JMS\Groups({"portal"})
     */
    protected $messageType;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("payload")
     * @JMS\Groups({"portal"})
     */
    protected $payload;


    /**
     * Delete constructor.
     */
    public function __construct()
    {
        $this->messageType = 'DELETE';
    }

    /**
     * @return string
     */
    public function getMessageType()
    {
        return $this->messageType;
    }

    /**
     * @return string
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param string $payload
     * @return $this
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;
        return $this;
    }
}
