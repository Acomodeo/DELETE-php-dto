<?php

namespace Aoo\Dto\ApartmentMessage;

use JMS\Serializer\Annotation as JMS;
use Aoo\Dto;

/**
 * Class Property
 * @package Aoo\Dto
 */
class Property
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("payloadType")
     * @JMS\Groups({"portal"})
     */
    protected $payloadType;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("messageType")
     * @JMS\Groups({"portal"})
     */
    protected $messageType;

    /**
     * @var Dto\Property
     * @JMS\Type("Aoo\Dto\Property")
     * @JMS\SerializedName("payload")
     * @JMS\Groups({"portal"})
     */
    protected $payload;

    /**
     * @return string
     */
    public function getPayloadType()
    {
        return $this->payloadType;
    }

    /**
     * Delete constructor.
     */
    public function __construct()
    {
        $this->payloadType = 'PROPERTY';
        $this->messageType = 'PUT';
    }

    /**
     * @return string
     */
    public function getMessageType()
    {
        return $this->messageType;
    }

    /**
     * @return Dto\Property
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @param Dto\Property $payload
     * @return $this
     */
    public function setPayload($payload)
    {
        $this->payload = $payload;
        return $this;
    }
}
