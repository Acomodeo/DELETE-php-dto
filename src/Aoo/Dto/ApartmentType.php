<?php

namespace Aoo\Dto;

use JMS\Serializer\Annotation as JMS;
use Aoo\Dto\Definitions;

/**
 * Class ApartmentType
 * @package Aoo\Dto
 */
class ApartmentType
{

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"portal"})
     */
    protected $id;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("propertyId")
     * @JMS\Groups({"portal"})
     */
    protected $propertyId;

    /**
     * @var Definitions\ChannelLink[]
     * @JMS\Type("array<string, Aoo\Dto\Definitions\ChannelLink>")
     * @JMS\SerializedName("channelLinks")
     * @JMS\Groups({"portal"})
     */
    protected $channelLinks;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("rating")
     * @JMS\Groups({"portal"})
     */
    protected $rating;

    /**
     * @var array
     * @JMS\Type("array<string>")
     * @JMS\SerializedName("includedServices")
     * @JMS\Groups({"portal"})
     */
    protected $includedServices;

    /**
     * @var Definitions\BookableService[]
     * @JMS\Type("array<string, Aoo\Dto\Definitions\BookableService>")
     * @JMS\SerializedName("bookableServices")
     * @JMS\Groups({"portal"})
     */
    protected $bookableServices;

    /**
     * @var Definitions\Size
     * @JMS\Type("Aoo\Dto\Definitions\Size")
     * @JMS\SerializedName("size")
     * @JMS\Groups({"portal"})
     */
    protected $size;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("bedrooms")
     * @JMS\Groups({"portal"})
     */
    protected $bedrooms;

    /**
     * @var array
     * @JMS\Type("array")
     * @JMS\SerializedName("beds")
     * @JMS\Groups({"portal"})
     */
    protected $beds;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("bathrooms")
     * @JMS\Groups({"portal"})
     */
    protected $bathrooms;

    /**
     * @var Definitions\LengthOfStay
     * @JMS\Type("Aoo\Dto\Definitions\LengthOfStay")
     * @JMS\SerializedName("lengthOfStay")
     * @JMS\Groups({"portal"})
     */
    protected $lengthOfStay;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("maxOccupancy")
     * @JMS\Groups({"portal"})
     */
    protected $maxOccupancy;

    /**
     * @var array
     * @JMS\Type("array<string,string>")
     * @JMS\SerializedName("name")
     * @JMS\Groups({"portal"})
     */
    protected $name;

    /**
     * @var array
     * @JMS\Type("array<string,string>")
     * @JMS\SerializedName("description")
     * @JMS\Groups({"portal"})
     */
    protected $description;

    /**
     * @var Definitions\Picture
     * @JMS\Type("Aoo\Dto\Definitions\Picture")
     * @JMS\SerializedName("thumbnail")
     * @JMS\Groups({"portal"})
     */
    protected $thumbnail;

    /**
     * @var Definitions\Picture[]
     * @JMS\Type("array<Aoo\Dto\Definitions\Picture>")
     * @JMS\SerializedName("pictures")
     * @JMS\Groups({"portal"})
     */
    protected $pictures;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = trim($id);
        return $this;
    }

    /**
     * @return string
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param string $propertyId
     * @return $this
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = trim($propertyId);
        return $this;
    }

    /**
     * @return Definitions\ChannelLink[]
     */
    public function getChannelLinks()
    {
        return $this->channelLinks;
    }

    /**
     * @param Definitions\ChannelLink[] $channelLinks
     * @return $this
     */
    public function setChannelLinks($channelLinks)
    {
        $this->channelLinks = $channelLinks;
        return $this;
    }

    /**
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param string $rating
     * @return $this
     */
    public function setRating($rating)
    {
        // only set allowed values
        if (in_array($rating, [
            'ECONOMY',
            'BUSINESS',
            'FIRST'
        ])) {
            $this->rating = $rating;
        } else {
            $this->rating = null;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getIncludedServices()
    {
        return $this->includedServices;
    }

    /**
     * @param array $includedServices
     * @return $this
     */
    public function setIncludedServices($includedServices)
    {
        $this->includedServices = array_values(array_filter($includedServices));
        return $this;
    }

    /**
     * @param $serviceName
     * @return bool
     */
    public function hasIncludedService($serviceName)
    {
        if ($this->includedServices) {
            foreach ($this->includedServices as $service) {
                if ($service === $serviceName) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return Definitions\BookableService[]
     */
    public function getBookableServices()
    {
        return $this->bookableServices;
    }

    /**
     * @param Definitions\BookableService[] $bookableServices
     * @return $this
     */
    public function setBookableServices($bookableServices)
    {
        $this->bookableServices = $bookableServices;
        return $this;
    }

    /**
     * @param $serviceName
     * @return bool
     */
    public function hasBookableService($serviceName)
    {
        if ($this->bookableServices) {
            foreach ($this->bookableServices as $service) {
                if ($service === $serviceName) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return Definitions\Size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param Definitions\Size $size
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return int
     */
    public function getBedrooms()
    {
        return $this->bedrooms;
    }

    /**
     * @param int $bedrooms
     * @return $this
     */
    public function setBedrooms($bedrooms)
    {
        $this->bedrooms = $bedrooms;
        return $this;
    }

    /**
     * @return array
     */
    public function getBeds()
    {
        return $this->beds;
    }

    /**
     * @param array $beds
     * @return $this
     */
    public function setBeds($beds)
    {
        $this->beds = $beds;
        return $this;
    }

    /**
     * @return int
     */
    public function getBathrooms()
    {
        return $this->bathrooms;
    }

    /**
     * @param int $bathrooms
     * @return $this
     */
    public function setBathrooms($bathrooms)
    {
        $this->bathrooms = $bathrooms;
        return $this;
    }

    /**
     * @return Definitions\LengthOfStay
     */
    public function getLengthOfStay()
    {
        return $this->lengthOfStay;
    }

    /**
     * @param Definitions\LengthOfStay $lengthOfStay
     * @return $this
     */
    public function setLengthOfStay($lengthOfStay)
    {
        $this->lengthOfStay = $lengthOfStay;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxOccupancy()
    {
        return $this->maxOccupancy;
    }

    /**
     * @param int $maxOccupancy
     * @return $this
     */
    public function setMaxOccupancy($maxOccupancy)
    {
        $this->maxOccupancy = $maxOccupancy;
        return $this;
    }

    /**
     * @return array
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param array $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param array $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Definitions\Picture
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param Definitions\Picture $thumbnail
     * @return $this
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    /**
     * @return Definitions\Picture[]
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * @param Definitions\Picture[] $pictures
     * @return $this
     */
    public function setPictures($pictures)
    {
        $this->pictures = $pictures;
        return $this;
    }


}
