<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class AcceptableGuarantee
 * @package Aoo\Dto\Definitions
 */
class AcceptableGuarantee
{

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("type")
     * @JMS\Groups({"portal"})
     */
    protected $type;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("amount")
     * @JMS\Groups({"portal"})
     */
    protected $amount;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type must be DEPOSIT, GUARANTEE or ACOMODEO
     * @return $this
     * @throws \OutOfBoundsException
     */
    public function setType($type)
    {
        if (
            ($type != 'DEPOSIT') &&
            ($type != 'GUARANTEE') &&
            ($type != 'ACOMODEO')
        ) {
            throw new \OutOfBoundsException('type must be DEPOSIT, GUARANTEE or ACOMODEO');
        }

        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

}