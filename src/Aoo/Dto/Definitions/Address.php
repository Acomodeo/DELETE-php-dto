<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Address
 * @package Aoo\Dto\Definitions
 */
class Address
{

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("label")
     * @JMS\Groups({"portal"})
     */
    protected $label;


    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("street")
     * @JMS\Groups({"portal"})
     */
    protected $street;


    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("city")
     * @JMS\Groups({"portal"})
     */
    protected $city;


    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("postalCode")
     * @JMS\Groups({"portal"})
     */
    protected $postalCode;


    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("country")
     * @JMS\Groups({"portal"})
     */
    protected $country;

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     * @return $this
     */
    public function setStreet($street)
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return $this
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }



}
