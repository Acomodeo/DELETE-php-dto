<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Size
 * @package Aoo\Dto\Definitions
 */
class BookableService
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"portal"})
     */
    protected $id;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("once")
     * @JMS\Groups({"portal"})
     */
    protected $once;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("perNight")
     * @JMS\Groups({"portal"})
     */
    protected $perNight;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("perPerson")
     * @JMS\Groups({"portal"})
     */
    protected $perPerson;

    /**
     * BookableService constructor.
     */
    public function __construct()
    {
        $this->once = 0;
        $this->perNight = 0;
        $this->perPerson = 0;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getOnce()
    {
        return $this->once;
    }

    /**
     * @param int $once
     * @return $this
     */
    public function setOnce($once)
    {
        $this->once = $once;
        return $this;
    }

    /**
     * @return int
     */
    public function getPerNight()
    {
        return $this->perNight;
    }

    /**
     * @param int $perNight
     * @return $this
     */
    public function setPerNight($perNight)
    {
        $this->perNight = $perNight;
        return $this;
    }

    /**
     * @return int
     */
    public function getPerPerson()
    {
        return $this->perPerson;
    }

    /**
     * @param int $perPerson
     * @return $this
     */
    public function setPerPerson($perPerson)
    {
        $this->perPerson = $perPerson;
        return $this;
    }


}


