<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class CancelPenalty
 * @package Aoo\Dto\Definitions
 */
class CancelPenalty
{

    /**
     * @var DeadlineRelative
     * @JMS\Type("<Aoo\Dto\Definitions\DeadlineRelative>")
     * @JMS\SerializedName("deadline")
     * @JMS\Groups({"portal"})
     */
    protected $deadline;

    /**
     * @var double
     * @JMS\Type("double")
     * @JMS\SerializedName("amount")
     * @JMS\Groups({"portal"})
     */
    protected $amount;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("details")
     * @JMS\Groups({"portal"})
     */
    protected $details;

    /**
     * @return DeadlineRelative
     */
    public function getDeadline()
    {
        return $this->deadline;
    }

    /**
     * @param DeadlineRelative $deadline
     * @return $this
     */
    public function setDeadline($deadline)
    {
        $this->deadline = $deadline;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param string $details
     * @return $this
     */
    public function setDetails($details)
    {
        $this->details = $details;
        return $this;
    }



}