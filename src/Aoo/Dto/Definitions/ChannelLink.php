<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class ChannelLink
 * @package Aoo\Dto\Definitions
 */
class ChannelLink
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"portal"})
     */
    protected $id;

    /**
     * @var array
     * @JMS\Type("array")
     * @JMS\SerializedName("discountCodes")
     * @JMS\Groups({"portal"})
     */
    protected $discountCodes;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return array
     */
    public function getDiscountCodes()
    {
        return $this->discountCodes;
    }

    /**
     * @param array $discountCodes
     * @return $this
     */
    public function setDiscountCodes($discountCodes)
    {
        $this->discountCodes = $discountCodes;
        return $this;
    }
}