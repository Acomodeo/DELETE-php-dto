<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class DeadlineRelative
 * @package Aoo\Dto\Definitions
 */
class DeadlineRelative
{

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("type")
     * @JMS\Groups({"portal"})
     */
    protected $type = "RELATIVE";

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("basedOn")
     * @JMS\Groups({"portal"})
     */
    protected $basedOn;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("offset")
     * @JMS\Groups({"portal"})
     */
    protected $offset;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("unit")
     * @JMS\Groups({"portal"})
     */
    protected $unit;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type must be RELATIVE
     * @return $this
     * @throws \OutOfBoundsException
     */
    public function setType($type)
    {
        if (
            ($type != 'RELATIVE')
        ) {
            throw new \OutOfBoundsException('type must be RELATIVE');
        }
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getBasedOn()
    {
        return $this->basedOn;
    }

    /**
     * @param string $basedOn must be BEFORE_ARRIVAL or AFTER_BOOKING
     * @return $this
     * @throws \OutOfBoundsException
     */
    public function setBasedOn($basedOn)
    {
        if (
            ($basedOn != 'BEFORE_ARRIVAL') &&
            ($basedOn != 'AFTER_BOOKING')
        ) {
            throw new \OutOfBoundsException('basedOn must be BEFORE_ARRIVAL or AFTER_BOOKING');
        }
        $this->basedOn = $basedOn;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     * @return $this
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     *
     * @param string $unit must be HOURS or DAYS
     * @return $this
     * @throws \OutOfBoundsException
     */
    public function setUnit($unit)
    {
        if (
            ($unit != 'HOURS') &&
            ($unit != 'DAYS')
        ) {
            throw new \OutOfBoundsException('unit must be HOURS or DAYS');
        }
        $this->unit = $unit;
        return $this;
    }

}