<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class LengthOfStay
 * @package Aoo\Dto\Definitions
 */
class LengthOfStay
{

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\Groups({"portal"})
     */
    protected $min;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\Groups({"portal"})
     */
    protected $max;

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param int $min
     * @return $this
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param int $max
     * @return $this
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

}
