<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Location
 * @package Aoo\Dto\Definitions
 */
class Location
{
    /**
     * @var float
     * @JMS\Type("double")
     * @JMS\Groups({"portal"})
     */
    protected $lat;

    /**
     * @var float
     * @JMS\Type("double")
     * @JMS\Groups({"portal"})
     */
    protected $long;

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     * @return $this
     */
    public function setLat($lat)
    {
        $lat = (double) $lat;
        if (abs($lat) > 90) {
            throw new \OutOfRangeException('latitude must be between -90 and 90');
        }
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return float
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * @param float $long
     * @return $this
     */
    public function setLong($long)
    {
        $long = (double) $long;
        if (abs($long) > 180) {
            throw new \OutOfRangeException('longitude must be between -180 and 180');
        }
        $this->long = $long;
        return $this;
    }
}
