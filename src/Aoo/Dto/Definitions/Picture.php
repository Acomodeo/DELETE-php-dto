<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Picture
 * @package Aoo\Dto\Definitions
 */
class Picture
{

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\Groups({"portal"})
     */
    protected $id;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

}