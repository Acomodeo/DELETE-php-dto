<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class Rate
 * @package Aoo\Dto\Definitions
 */
class Rate
{
    /**
     * @var TimeSpan
     * @JMS\Type("Aoo\Dto\Definitions\TimeSpan")
     * @JMS\SerializedName("timeSpan")
     * @JMS\Groups({"portal"})
     */
    protected $timeSpan;

    /**
     * @var double
     * @JMS\Type("double")
     * @JMS\SerializedName("price")
     * @JMS\Groups({"portal"})
     */
    protected $price;

    /**
     * @var LengthOfStay
     * @JMS\Type("Aoo\Dto\Definitions\LengthOfStay")
     * @JMS\SerializedName("lengthOfStay")
     * @JMS\Groups({"portal"})
     */
    protected $lengthOfStay;

    /**
     * @return TimeSpan
     */
    public function getTimeSpan()
    {
        return $this->timeSpan;
    }

    /**
     * @param TimeSpan $timeSpan
     * @return $this
     */
    public function setTimeSpan($timeSpan)
    {
        $this->timeSpan = $timeSpan;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return LengthOfStay
     */
    public function getLengthOfStay()
    {
        return $this->lengthOfStay;
    }

    /**
     * @param LengthOfStay $lengthOfStay
     * @return $this
     */
    public function setLengthOfStay($lengthOfStay)
    {
        $this->lengthOfStay = $lengthOfStay;
        return $this;
    }

}
