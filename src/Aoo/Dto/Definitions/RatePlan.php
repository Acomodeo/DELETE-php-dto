<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class RatePlan
 * @package Aoo\Dto\Definitions
 */
class RatePlan
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("ratePlanId")
     * @JMS\Groups({"portal"})
     */
    protected $ratePlanId;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("apartmentTypeId")
     * @JMS\Groups({"portal"})
     */
    protected $apartmentTypeId;

    /**
     * @var array
     * @JMS\Type("array<string>")
     * @JMS\SerializedName("apartments")
     * @JMS\Groups({"portal"})
     */
    protected $apartments = [];

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("state")
     * @JMS\Groups({"portal"})
     */
    protected $state;

    /**
     * @var array
     * @JMS\Type("array<string,string>")
     * @JMS\SerializedName("name")
     * @JMS\Groups({"portal"})
     */
    protected $name;

    /**
     * @var array
     * @JMS\Type("array<string,string>")
     * @JMS\SerializedName("description")
     * @JMS\Groups({"portal"})
     */
    protected $description;

    /**
     * @var Rate[]
     * @JMS\Type("array<Aoo\Dto\Definitions\Rate>")
     * @JMS\SerializedName("rates")
     * @JMS\Groups({"portal"})
     */
    protected $rates;

    /**
     * @var CancelPenalty[]
     * @JMS\Type("array<Aoo\Dto\Definitions\CancelPenalty>")
     * @JMS\SerializedName("cancelPenalties")
     * @JMS\Groups({"portal"})
     */
    protected $cancelPenalties;

    /**
     * @var AdvancedBookingOffset
     * @JMS\Type("Aoo\Dto\Definitions\AdvancedBookingOffset")
     * @JMS\SerializedName("advancedBookingOffset")
     * @JMS\Groups({"portal"})
     */
    protected $advancedBookingOffset;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("currency")
     * @JMS\Groups({"portal"})
     */
    protected $currency;
    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("maxOccupancy")
     * @JMS\Groups({"portal"})
     */
    protected $maxOccupancy;

    /**
     * @var array
     * @JMS\Type("array<string>")
     * @JMS\SerializedName("includedServices")
     * @JMS\Groups({"portal"})
     */
    protected $includedServices;

    /**
     * @var AcceptableGuarantee[]
     * @JMS\Type("array<Aoo\Dto\Definitions\AcceptableGuarantee>")
     * @JMS\SerializedName("acceptableGuarantees")
     * @JMS\Groups({"portal"})
     */
    protected $acceptableGuarantees;

    /**
     * @return string
     */
    public function getRatePlanId()
    {
        return $this->ratePlanId;
    }

    /**
     * @param string $ratePlanId
     * @return $this
     */
    public function setRatePlanId($ratePlanId)
    {
        $this->ratePlanId = $ratePlanId;
        return $this;
    }

    /**
     * @return string
     */
    public function getApartmentTypeId()
    {
        return $this->apartmentTypeId;
    }

    /**
     * @param string $apartmentTypeId
     * @return $this
     */
    public function setApartmentTypeId($apartmentTypeId)
    {
        $this->apartmentTypeId = $apartmentTypeId;
        return $this;
    }

    /**
     * @return array
     */
    public function getApartments()
    {
        return $this->apartments;
    }

    /**
     * @param array $apartments
     * @return $this
     */
    public function setApartments($apartments)
    {
        $this->apartments = $apartments;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxOccupancy()
    {
        return $this->maxOccupancy;
    }

    /**
     * @param int $maxOccupancy
     * @return $this
     */
    public function setMaxOccupancy($maxOccupancy)
    {
        $this->maxOccupancy = $maxOccupancy;
        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return strtoupper($this->state);
    }

    /**
     * @param string $state needs to be ONLINE or OFFLINE
     * @return $this
     */
    public function setState($state)
    {
        if (
            ($state != 'ONLINE') &&
            ($state != 'OFFLINE')
        ) {
            throw new \OutOfBoundsException('state needs to be ONLINE or OFFLINE');
        }
        $this->state = strtoupper($state);
        return $this;
    }

    /**
     * @return array
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param array $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return array
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param array $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Rate[]
     */
    public function getRates()
    {
        return $this->rates;
    }

    /**
     * @param Rate[] $rates
     * @return $this
     */
    public function setRates($rates)
    {
        $this->rates = $rates;
        return $this;
    }

    /**
     * @return CancelPenalty[]
     */
    public function getCancelPenalties()
    {
        return $this->cancelPenalties;
    }

    /**
     * @param CancelPenalty[] $cancelPenalties
     * @return $this
     */
    public function setCancelPenalties($cancelPenalties)
    {
        $this->cancelPenalties = $cancelPenalties;
        return $this;
    }

    /**
     * @return AdvancedBookingOffset
     */
    public function getAdvancedBookingOffset()
    {
        return $this->advancedBookingOffset;
    }

    /**
     * @param AdvancedBookingOffset $advancedBookingOffset
     * @return $this
     */
    public function setAdvancedBookingOffset($advancedBookingOffset)
    {
        $this->advancedBookingOffset = $advancedBookingOffset;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return array
     */
    public function getIncludedServices()
    {
        return $this->includedServices;
    }

    /**
     * @param array $includedServices
     * @return $this
     */
    public function setIncludedServices($includedServices)
    {
        $this->includedServices = $includedServices;
        return $this;
    }

    /**
     * @return AcceptableGuarantee[]
     */
    public function getAcceptableGuarantees()
    {
        return $this->acceptableGuarantees;
    }

    /**
     * @param AcceptableGuarantee[] $acceptableGuarantees
     * @return $this
     */
    public function setAcceptableGuarantees($acceptableGuarantees)
    {
        $this->acceptableGuarantees = $acceptableGuarantees;
        return $this;
    }


}
