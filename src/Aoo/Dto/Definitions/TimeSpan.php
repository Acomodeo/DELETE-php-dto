<?php

namespace Aoo\Dto\Definitions;

use JMS\Serializer\Annotation as JMS;

/**
 * Class TimeSpan
 * @package Aoo\Dto\Definitions
 */
class TimeSpan
{
    /**
     * @var \DateTime
     * @JMS\Type("DateTime<'dmY'>")
     * @JMS\SerializedName("from")
     * @JMS\Groups({"portal"})
     */
    protected $from;

    /**
     * @var \DateTime
     * @JMS\Type("DateTime<'dmY'>")
     * @JMS\SerializedName("to")
     * @JMS\Groups({"portal"})
     */
    protected $to;

    /**
     * @return \DateTime
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param \DateTime $from
     * @return $this
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param \DateTime $to
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

}