<?php

namespace Aoo\Dto\Envelopes;

use JMS\Serializer\Annotation as JMS;
use Aoo\Dto\Definitions\RatePlan;

/**
 * Class PropertyRatePlans
 * @package Aoo\Dto\Envelopes
 */
class PropertyRatePlans
{
    /**
     * @var \DateTime
     * @JMS\Type("DateTime<'Uu'>")
     * @JMS\SerializedName("lastUpdated")
     * @JMS\Groups({"portal"})
     */
    protected $lastUpdated;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("propertyId")
     * @JMS\Groups({"portal"})
     */
    protected $propertyId;

    /**
     * @var RatePlan[]
     * @JMS\Type("array<Aoo\Dto\Definitions\RatePlan>")
     * @JMS\SerializedName("apartmentUpdates")
     * @JMS\Groups({"portal"})
     */
    protected $apartmentUpdates;

    /**
     * @return \DateTime
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * @param \DateTime $lastUpdated
     * @return $this
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;
        return $this;
    }

    /**
     * @return string
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * @param string $propertyId
     * @return $this
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;
        return $this;
    }

    /**
     * @return \Aoo\Dto\Definitions\RatePlan[]
     */
    public function getApartmentUpdates()
    {
        return $this->apartmentUpdates;
    }

    /**
     * @param \Aoo\Dto\Definitions\RatePlan[] $apartmentUpdates
     * @return $this
     */
    public function setApartmentUpdates($apartmentUpdates)
    {
        $this->apartmentUpdates = $apartmentUpdates;
        return $this;
    }


}