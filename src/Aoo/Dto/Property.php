<?php

namespace Aoo\Dto;

use JMS\Serializer\Annotation as JMS;
use Aoo\Dto\Definitions;

/**
 * Class Property
 * @package Aoo\Dto
 */
class Property
{
    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("id")
     * @JMS\Groups({"portal"})
     */
    protected $id;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("chainId")
     * @JMS\Groups({"portal"})
     */
    protected $chainId;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("hotelId")
     * @JMS\Groups({"portal"})
     */
    protected $hotelId;

    /**
     * @var array
     * @JMS\Type("array<string,string>")
     * @JMS\SerializedName("name")
     * @JMS\Groups({"portal"})
     */
    protected $name;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("apartmentsAmount")
     * @JMS\Groups({"portal"})
     */
    protected $apartmentsAmount;

    /**
     * @var ApartmentType[]
     * @JMS\Type("array<Aoo\Dto\ApartmentType>")
     * @JMS\SerializedName("apartmentTypes")
     * @JMS\Groups({"portal"})
     */
    protected $apartmentTypes;

    /**
     * @var Definitions\Location
     * @JMS\Type("Aoo\Dto\Definitions\Location")
     * @JMS\SerializedName("location")
     * @JMS\Groups({"portal"})
     */
    protected $location;

    /**
     * @var boolean
     * @JMS\Type("boolean")
     * @JMS\SerializedName("acomodeoPayment")
     * @JMS\Groups({"portal"})
     */
    protected $acomodeoPayment;

    /**
     * @var array
     * @JMS\Type("array<string>")
     * @JMS\SerializedName("includedServices")
     * @JMS\Groups({"portal"})
     */
    protected $includedServices;

    /**
     * @var Definitions\BookableService[]
     * @JMS\Type("array<string, Aoo\Dto\Definitions\BookableService>")
     * @JMS\SerializedName("bookableServices")
     * @JMS\Groups({"portal"})
     */
    protected $bookableServices;

    /**
     * @var Definitions\Picture[]
     * @JMS\Type("array<Aoo\Dto\Definitions\Picture>")
     * @JMS\SerializedName("pictures")
     * @JMS\Groups({"portal"})
     */
    protected $pictures;

    /**
     * @var Definitions\Picture
     * @JMS\Type("Aoo\Dto\Definitions\Picture")
     * @JMS\SerializedName("chainLogo")
     * @JMS\Groups({"portal"})
     */
    protected $chainLogo;

    /**
     * @var array
     * @JMS\Type("array")
     * @JMS\SerializedName("descriptions")
     * @JMS\Groups({"portal"})
     */
    protected $descriptions;

    /**
     * @var Definitions\Address
     * @JMS\Type("Aoo\Dto\Definitions\Address")
     * @JMS\SerializedName("locationAddress")
     */
    protected $locationAddress;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("phone")
     * @JMS\Groups({"portal"})
     */
    protected $phone;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("email")
     * @JMS\Groups({"portal"})
     */
    protected $email;

    /**
     * @var string
     * @JMS\Type("string")
     * @JMS\SerializedName("emailReservation")
     * @JMS\Groups({"portal"})
     */
    protected $emailReservation;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("buildYear")
     * @JMS\Groups({"portal"})
     */
    protected $buildYear;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("renovationYear")
     * @JMS\Groups({"portal"})
     */
    protected $renovationYear;

    /**
     * @var integer
     * @JMS\Type("integer")
     * @JMS\SerializedName("floors")
     * @JMS\Groups({"portal"})
     */
    protected $floors;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = trim($id);
        return $this;
    }

    /**
     * @return string
     */
    public function getChainId()
    {
        return $this->chainId;
    }

    /**
     * @param string $chainId
     * @return $this
     */
    public function setChainId($chainId)
    {
        $this->chainId = trim($chainId);
        return $this;
    }

    /**
     * @return string
     */
    public function getHotelId()
    {
        return $this->hotelId;
    }

    /**
     * @param string $hotelId
     * @return $this
     */
    public function setHotelId($hotelId)
    {
        $this->hotelId = trim($hotelId);
        return $this;
    }

    /**
     * @return array
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param array $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getApartmentsAmount()
    {
        return $this->apartmentsAmount;
    }

    /**
     * @param int $apartmentsAmount
     * @return $this
     */
    public function setApartmentsAmount($apartmentsAmount)
    {
        $this->apartmentsAmount = $apartmentsAmount;
        return $this;
    }

    /**
     * @return ApartmentType[]
     */
    public function getApartmentTypes()
    {
        return $this->apartmentTypes;
    }

    /**
     * @param ApartmentType[] $apartmentTypes
     * @return $this
     */
    public function setApartmentTypes($apartmentTypes)
    {
        $this->apartmentTypes = $apartmentTypes;
        return $this;
    }

    /**
     * @param ApartmentType $apartmentType
     * @return $this
     */
    public function addApartmentType($apartmentType)
    {
        $this->apartmentTypes[] = $apartmentType;
        return $this;
    }

    /**
     * @return Definitions\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param Definitions\Location $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isAcomodeoPayment()
    {
        return $this->acomodeoPayment;
    }

    /**
     * @param boolean $acomodeoPayment
     * @return $this
     */
    public function setAcomodeoPayment($acomodeoPayment)
    {
        $this->acomodeoPayment = $acomodeoPayment;
        return $this;
    }

    /**
     * @return array
     */
    public function getIncludedServices()
    {
        return $this->includedServices;
    }

    /**
     * @param array $includedServices
     * @return $this
     */
    public function setIncludedServices($includedServices)
    {
        if (!is_array($includedServices)) {
            $includedServices = [];
        }
        $this->includedServices = array_values(array_filter($includedServices));
        return $this;
    }

    /**
     * @return Definitions\BookableService[]
     */
    public function getBookableServices()
    {
        return $this->bookableServices;
    }

    /**
     * @param Definitions\BookableService[] $bookableServices
     * @return $this
     */
    public function setBookableServices($bookableServices)
    {
        $this->bookableServices = $bookableServices;
        return $this;
    }

    /**
     * @return Definitions\Picture[]
     */
    public function getPictures()
    {
        return $this->pictures;
    }

    /**
     * @param Definitions\Picture[] $pictures
     * @return $this
     */
    public function setPictures($pictures)
    {
        $this->pictures = $pictures;
        return $this;
    }

    /**
     * @return Definitions\Picture
     */
    public function getChainLogo()
    {
        return $this->chainLogo;
    }

    /**
     * @param Definitions\Picture $chainLogo
     * @return $this
     */
    public function setChainLogo($chainLogo)
    {
        $this->chainLogo = $chainLogo;
        return $this;
    }

    /**
     * @return array
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * @param array $descriptions
     * @return $this
     */
    public function setDescriptions($descriptions)
    {
        $this->descriptions = $descriptions;
        return $this;
    }

    /**
     * @return Definitions\Address
     */
    public function getLocationAddress()
    {
        return $this->locationAddress;
    }

    /**
     * @param Definitions\Address $locationAddress
     * @return $this
     */
    public function setLocationAddress($locationAddress)
    {
        $this->locationAddress = $locationAddress;
        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\SerializedName("address")
     * @JMS\Groups({"portal"})
     *
     * @return string
     */
    public function getAddress()
    {
        return trim(
            $this->getLocationAddress()->getLabel() . "\n" .
            $this->getLocationAddress()->getStreet() . "\n" .
            trim($this->getLocationAddress()->getCountry() . ' ' . $this->getLocationAddress()->getPostalCode() . ' ' . $this->getLocationAddress()->getCity()) . "\n");
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = trim($phone);
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = trim($email);
        return $this;
    }

    /**
     * @return string
     */
    public function getEmailReservation()
    {
        return $this->emailReservation;
    }

    /**
     * @param string $emailReservation
     * @return $this
     */
    public function setEmailReservation($emailReservation)
    {
        $this->emailReservation = trim($emailReservation);
        return $this;
    }

    /**
     * @return int
     */
    public function getBuildYear()
    {
        return $this->buildYear;
    }

    /**
     * @param int $buildYear
     * @return $this
     */
    public function setBuildYear($buildYear)
    {
        $this->buildYear = $buildYear;
        return $this;
    }

    /**
     * @return int
     */
    public function getRenovationYear()
    {
        return $this->renovationYear;
    }

    /**
     * @param int $renovationYear
     * @return $this
     */
    public function setRenovationYear($renovationYear)
    {
        $this->renovationYear = $renovationYear;
        return $this;
    }

    /**
     * @return int
     */
    public function getFloors()
    {
        return $this->floors;
    }

    /**
     * @param int $floors
     * @return $this
     */
    public function setFloors($floors)
    {
        $this->floors = $floors;
        return $this;
    }


}
